\documentclass[sectionprefix=true]{scrartcl}
\usepackage[ngerman, english]{babel}	
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[linesnumbered, vlined, ruled, boxed]{algorithm2e}

\usepackage{amsfonts}
\newcommand{\bbfamily}{\fontencoding{U}\fontfamily{bbold}\selectfont}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

\usepackage{graphicx}
\usepackage{paralist}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{color}   
\usepackage{float}
\usepackage{appendix}
\usepackage{pdfpages}
\usepackage{listings}
\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=Java,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{black},
  commentstyle=\itshape\color{blue},
  identifierstyle=\color{black},
  stringstyle=\color{green},
  stepnumber = 1,
  numbers = left,
  extendedchars=true,
}

\usepackage{framed}
\usepackage{geometry}
\usepackage{fancyhdr}
\usepackage{xparse}
\usepackage{kantlipsum}

\usepackage{titlesec}
\titleformat{\section}[frame]
	{\huge\scshape\sffamily}
	{\filright
	 \footnotesize
	 \enspace Section \thesection \enspace}
	{8pt}
	{\huge\scshape\filcenter}
\titleformat{\subsection}[block]
	{\scshape\LARGE\filcenter}
	{\thesubsection.}
	{0.5em}{}
\titleformat{\subsubsection}[block]
	{\normalsize\scshape}
	{\thesubsubsection.}
	{0.5em}{}
  
\usepackage[tocflat]{tocstyle}
\usetocstyle{standard}


\pagestyle{plain}

\makeatletter
\NewDocumentCommand\headerspdf{ O {pages=-} m }{% [options for include pdf]{filename.pdf}
  \includepdf[%
    #1,
    pagecommand={\thispagestyle{fancy}},
    scale=.7,
    ]{#2}}
\NewDocumentCommand\secpdf{somO{1}m}{% [short title]{section title}[page specification]{filename.pdf} --- possibly starred
  \clearpage
  \thispagestyle{fancy}%
  \includepdf[%
    pages=#4,
    pagecommand={%
      \IfBooleanTF{#1}{%
        \section*{#3}}{%
        \IfNoValueTF{#2}{%
          \section{#3}}{%
          \section[#2]{#3}}}},
    scale=.65,
    ]%
    {#5}}
\makeatother

\usepackage{multind}
\makeindex{defn}
\makeindex{thm}
\makeindex{lem}
\makeindex{cor}
\makeindex{prop}
\makeindex{rem}
\makeindex{ex}
\makeindex{alg}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true, 
    linktoc=all,
    linkcolor=black,
}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{prop}{Proposition}[section]

\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{ex}{Example}[section]

\theoremstyle{remark}
\newtheorem{rem}{Remark}[section]

\newcommand{\PP}{\mathcal P}
\newcommand{\NP}{$\mathcal{NP}$ }
\newcommand{\X}{\mathcal X}
\newcommand{\Prob}{\mathbf{Pr}}
\newcommand{\PSpace}{(\Omega, \Prob)}
\newcommand{\Var}{\text{Var}}
\newcommand{\NPshow}{ $\in $ \NP $\checkmark$}
\newcommand{\QS}{$\mathtt{QuickSort}$}
\newcommand{\bin}{$\mathtt{bin}$}
\newcommand{\ZPP}{$\mathcal{ZPP}$ }
\newcommand{\dfs}{$\mathtt{DFS}$ }
\newcommand{\bug}{\texttt{BUG}}

\begin{document}

\begin{titlepage}
	\centering
	{\scshape\LARGE Summary \par}
	\vspace{1cm}
	{\scshape\huge Discrete and Computational Geometry\par}
	\vspace{1cm}
	{\Large\itshape Fabian Brand\par}
	{\Large\itshape Bonn, \today\par}
	\vfill
	\begin{abstract}
This document contains a comprehensive summary of the topics discussed in the lecture \glqq Discrete and Computational Geometry\grqq. The lecture was given at the University of Bonn during the winter term 2016/17. This is an inofficial summary of the topics discussed and was written in preparation for the exam and to personally repeat the contents of the lecture. As such, the notation might be inconsistent and there might be severe errors.
\end{abstract}
\end{titlepage}


\title{Discrete and Computational Geometry}
\subtitle{Winter term 2016/17}
\author{Fabian Brand}

\lstset{language=Java,style=customc, literate=%
    {Ö}{{\"O}}1
    {Ä}{{\"A}}1
    {Ü}{{\"U}}1
    {ß}{{\ss}}1
    {ü}{{\"u}}1
    {ä}{{\"a}}1
    {ö}{{\"o}}1
    {~}{{\textasciitilde}}1}
%\maketitle
%\pagebreak

\pagebreak
\tableofcontents
\pagebreak

\section{Spanners}
The first lectures covered the topic of spanners. In very broad terms, we will discuss ways to find, for a set of points in the euclidean plane (i.e. cities on a map), a suitable network of roads to connect those cities.
\begin{leftbar}
	\begin{description} 
		\item[Input:] $n$ points in the euclidean plane. 
		\item[Task:] Find a good network connecting the points (i.e. a network with low dilation, few edges and low total weight).
	\end{description}
\end{leftbar}
An important criterion for a good network is dilation, which is the deviation from the length of the shortest path in the plane compared to the shortest path in the network.
\begin{defn}[Dilation]\index{defn}{Dilation} Let $S$ be a set of points on the euclidean plane and let $p,q \in S$ be two points. Let $\pi_N(p,q)$ be the length of a shortest path between $p$ and $q$ in a network $N$. Let $|pq|$ be the euclidean distance between the two points. Then, \[
	\delta_N(p,q) := \frac{|\pi_N(p,q)|}{|pq|}
\]
is called the dilation of $p$ and $q$. Similarly, we call \[
	\delta(N) := \max_{p,q \in S} \delta_N(p,q)
\]
dilation of the network $N$.
\end{defn}
For the extreme cases of the dilation, we can easily find some examples. If we want to achieve $\delta = 1$ for a network we can construct the complete graph on the input points. Conversely, to use as few edges as possible, we can use trees. The Minimum spanning tree behaves like expected but a Steinertree has proven to give better results. But as they have few edges, we can prove the following theorem concerning the dilation of trees.
\begin{thm}[Dilation of Trees]\index{thm}{Dilation of Trees} Let $S_n$ be a set of $n$ points evenly placed on a circle $C$. Every spanning tree $T$ for $S$ is of dilation \[
	\delta(T) \geq \frac{n}{\pi}.
\]
\end{thm}
\begin{proof} Suppose there exists a tree $T$ with $\delta(T) < \frac{n}{\pi} \leq \frac{1}{\sin(\frac{\pi}{n})}$. \par
\begin{figure}[] \centering \includegraphics[width=11cm]{circle_ellipse_triangle} \label{fig:circle_ellipse} \caption{The ellipse $E$ based on the points $p_i$ and $p_{i+1}$ and the circle $C$.}\end{figure} 
Let $p_i, p_{i + 1}$ be two successive points on the circle. Let $E$ be an ellipse surrounding the center of the circle and both points (\autoref{fig:circle_ellipse}). Then, $E$ is the locus of all points $z$ where \[
	|zp_i| + |zp_{i+1}| = 2
\]
Let $\pi_i^{i+1}$ be the shortest path in the tree $T$ connecting $p_i$ to $p_{i+1}$. With this, we can prove $\pi_i^{i+1} \subseteq E$. Let $v$ be a vertex of $\pi_i^{i+1}$. 
\begin{align*}
	\frac{|p_iv| + |p_{i+1}v|}{2\sin(\alpha)} &= \frac{|p_iv| + |p_{i+1}v|}{|p_ip_{i+1}|} \\
	&=\delta(T) \\
	&< \frac{1}{\sin(\alpha)}
\end{align*}
Thus, we also know that $|p_iv| + |p_{i+1}v| \leq 2$, which in turn implies that the claim is true. This can be applied to all successive pairs of points in $S_n$. Then we have a closed cycle in $T$ (which proves the contradiction).
\end{proof}
\subsection{Well Separated Pair Decomposition}
To find a sophisticated algorithm to create spanner networks we will use the well separated pair decomposition. Before we can define the decomposition, we have to defined the meaning of two point sets being well separated.
\begin{defn}[Well Separated] \index{defn}{Well Separated} Let $A$ and $B$ two point sets. $A$ and $B$ are called well separated with respect to $s \geq 1$ if and only if the two circles $C_A, C_B$ covering the bounding boxes of $A$ and $B$ respectively are at least distance $s \cdot \max{r_{C_A}, r_{C_B}}$ apart.
\end{defn}
For the points contained in the sets $A$ and $B$, we can prove the following lemma.
\begin{lem}\label{lem:pointSeparation} \index{lem}{Lemma \ref{lem:pointSeparation}} Let $a, a' \in A$ and $b, b' \in B$ two points of each set and $A$ and $B$ well separated. Then,
\begin{enumerate}
	\item $|aa'| \leq 2r \leq 2\frac{|C_AC_B|}{s} \leq \frac{2}{s}|ab|$ and
	\item $|a'b'| \leq |a'a| + |ab| + |bb'| \leq (1 + \frac{4}{s})|ab|$.
\end{enumerate} \end{lem}
With this, we can define the well separated pair decomposition.
\begin{defn}[Well Separated Pair Decomposition] \index{defn}{Well Separated Pair Decomposition} A well separated pair decomposition of a set $S$ for given separation parameter $s$ is a sequence of pairs $(A_1,B_1),...,(A_m, B_m)$, where $A_i, B_i \subseteq S$ such that for all $1 \leq i \leq m$, $A_i$ and $B_i$ are well separated with respect to $s$ and for all $p \neq q \in S$ there is exactly one $i$, such that $p \in A_i, q_in B_i$ (or vice versa).
\end{defn}
An application of the well separated pair decomposition is finding the closest pair of points in $S$. Let $p,q$ be this closest pair. By the second property, we have an index $i$ with $|A_i| = |B_i| = 1$. To find such a solution we can inspect each pair. Also, we can enforce the conditions of the well separated pair decomposition by taking all $\begin{pmatrix}n \\ 2 \end{pmatrix}$ pairs. But there is a more sophisticated algorithm to find a nice well separated pair decomposition.
\begin{thm}[Creating a well separated pair decomposition] \index{thm}{Creating a well separated pair decomposition} Given a set $S$ of $n$ points in $\mathbb R^d$ and a separation parameter $s$, a well separated pair decomposition can be found in time \[
	O(d \cdot n\log(n) + (2s)^dd^{\frac{d}{2}}m) = O(n\log(n)).
\]
The number of pairs is in $O((2s)^dd^{\frac{d}{2}}n)$.
\end{thm}
\begin{proof} We will prove this theorem in two steps. First, we will describe the algorithm that is used to construct the well separated pair decomposition and prove its correctness as well as running time. Secondly, we will show that any point set can occur only a set number of times.

\IncMargin{1em}
\begin{algorithm}
\DontPrintSemicolon
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{splittree}{Split-Tree}\SetKwFunction{findpairs}{Find-Pairs}
\SetKwProg{myalg}{}{:}{}
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

\Input{A set of points $S$ on the euclidean plane}
\Output{A well separated pair decomposition $(A_1,B_1),...,(A_m, B_m)$}

\splittree{(S)}\;
Call \findpairs{(v,w)} for certain nodes of the split tree.\;

\caption{The outline of the algorithm used to compute a well separated pair decomposition}
\label{alg:wspd}
\end{algorithm}
\DecMargin{1em}

We will begin by discussing how to construct the split-tree. This is done by recursively splitting the bounding box of the set at its unique largest outside. The bounding boxes are always split at the half-way point of the respective edge, regardless of the number of points in either half. Practically, this can be done by sorting the coordinates of the points and storing this information in a linked list. Then we can build the tree in $\log(n)$ many rounds. To do this, in each round we want to half the number of points in the sets. For example, in round 1 we split until each intermediate leaf contains at most $\frac{n}{2}$ points. Since one of the intermediate leafs created at each level will not be touched again and because the size of the smaller set is always at least one, we know that one round takes time at most $O(n)$. \par
Let $b,c$ be two nodes of the split tree $T$. Then, two cases might occur.
\begin{description}
	\item[Case 1:] $b, c$ are on the same path in $T$. \par Then, $b$ is above $c$ and therefore \[
		S_c \subset S_b \text{ and } R(S_c) \subset R(S_b)
	\]
	\item[Case 2:] $b, c$ are on different paths in $T$. \par Then, we have \[
		R(S_c) \cap R(S_b) = \emptyset
	\]
\end{description}
With this, we can note the following intermediate lemma about the split tree, which we have proven above.
\begin{lem} \index{lem}{Split Tree construction} The split tree $T$ can be constructed in time $O(n \log(n))$. \end{lem}

Now, we will invoke \texttt{Find-Pairs}$(v,w)$ for each node $u \in T$ with children $v,w$. Let $l_{\max}$ be the length of the longest edge of a rectangle.

\IncMargin{1em}
\begin{algorithm}
\DontPrintSemicolon
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{splittree}{Split-Tree}\SetKwFunction{findpairs}{Find-Pairs}
\SetKwProg{myalg}{}{:}{}
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

\Input{Two nodes $v,w$ of the split tree.}
\Output{The well separated pair decomposition of the sub-tree under $v,w$.}

\BlankLine

\If{$S_v,S_w$ are well separated}{
	Add $(S_v, S_w)$ to the well separated pair decomposition.\;
}
\ElseIf{$l_{\max}(R(S_v)) > l_{\max}(R(S_w))$}{
	$v_l, v_r \leftarrow $ left and right childs of $v$.\;
	\findpairs{$(v_l,w)$}\;
	\findpairs{$(v_r,w)$}\;
}
\tcc{$l_{\max}(R(S_v)) \leq l_{\max}(R(S_w))$}
\Else() {
	$w_l, w_r \leftarrow $ left and right childs of $w$.\;
	\findpairs{$(v,w_l)$}\;
	\findpairs{$(v,w_r)$}\;
}

\caption{The \texttt{Find-Pairs} subroutine.}
\label{alg:findpairs}
\end{algorithm}
\DecMargin{1em}

Tragically, we can observe easy examples where some sets occur more than once in the well separated pair decomposition. But, before we will bound the number of occurrences of a single set we will show that this algorithm is indeed correct. The proof for this can be roughly sketched as follows. Obviously, the algorithm terminates. Let $p$ and $q$ be two leaf nodes of the split tree. Then, the covering property is fulfilled because the recursion can only occur once on the respective paths. The uniqueness property is fulfilled by the same argument (we will never output true for a pair with the same mate for a child and its predecessor node). \par
Now, it remains to show that $O((2s)^dd^{\frac{d}{2}}n)$ many point sets are reported by \texttt{Find-Pairs}. Suppose $(a, b)$ gets reported. Let $\pi(a)$ be the predecessor if $a$ in $T$. Then either $(\pi(a), b)$ or $(a, \pi(b))$ are not well separated. Since we called the recursion on the path to $a$, we know \[
	l_{\max}(R(b)) \leq l_{\max}(R(\pi(a)))
\]
and therefore
\begin{align*}
	l_{\max}(R(\pi(a)) \leq ... &\leq l_{\max}R(\pi^k(a)) \\
	&\leq l_{\max}(R(\pi(b))).
\end{align*}
Then, we can draw circles $C_a, C_b$ with the radius \[
	r := \frac{\sqrt{d}}{2}l_{\max}(R(\pi(a)))
\]
around the centers $x$ and $y$ of $R(\pi(a))$ and $R(b)$. By the definition of $r$, both bounding boxes are fully contained in their respective circles. For $C_a$ the radius is exactly computed as one half of a diagonal of $R(\pi(a))$ and $R(b)$ is smaller thatn $R(\pi(a))$ as proven above. With the following case distinction we can now show that $|xy| < (\frac{s}{2}+1)2r$.
\begin{description}
	\item[Case 1:]$C_a \cap C_b = \emptyset$ \par Then, we know $|C_aC_b| < sr$. In turn, this implies \[
		|xy| = |C_aC_b| + 2r < (\frac{s}{2} + 1)2r.
	\]
	\item[Case 2:]$C_a \cap C_b \neq \emptyset$ \par Then, we can directly compute the result: \[
		|xy| \leq 2r < (\frac{s}{2} + 1)2r
	\]
\end{description}
Therefore, $y$ is contained in a hypercube $\mathcal U$ of size $2(\frac{s}{2} + 1)2r$ centered at $x$. Suppose $a$ has many matches $(a,b_1), (a,b_2),...$ in the well separated pair decomposition. Then, $R(b_1), R(b_2),...$ are disjoint. Let $R_0(S)$ be the smallest Hypercube that fully contains $R(S)$. Let $R_0(a)$ and $R_0(b)$ be the parts of $R_0(S)$ that contain the two child nodes. Then, if a node is not the root we get \[
	l_{\min}(R_0(b)) \geq \frac{1}{2} l_{\max}(R(\pi(b))).
\]
This can be proven by induction on the level of the tree $b$ is on.
\begin{description}
	\item[Start:] $\pi(b) = \text{root}$ \par
	\begin{align*}
		\Rightarrow l_{\min}(R_0(b)) &= \frac{1}{2}C(R_0(S)) \\
		&= \frac{1}{2}l_{\max}(R(S))
	\end{align*}
	\item[Step:] If $\pi(b)$ is not the root, then we can define some cases to prove the statement.
	\begin{description}
		\item[$l_{\min}(R_0(b)) = l_{\min}(R_0(\pi(b)))$:]
		\[
			\underbrace{\geq}_{\text{I.H.}} \frac{1}{2}l_{\max}(\pi^2(b)) \geq \frac{1}{2}l_{\max}(R(\pi(b)))
		\]
		\item[$l_{\min}(R_0(b)) < l_{\min}(R_0(\pi(b)))$:]
		Therefore, we get a new split dimension with the following size.
		\[
			l_{\min}(R_0(b)) = l_i(R_0(b)) \geq \frac{1}{2}l_{\max}(R(\pi(b)))
		\]
	\end{description}
\end{description}
Now we will again consider all different sets. Since all $R_0(b_i)$ are disjoint, we get \[
	l_{\max}(R(\pi(a))) \leq l_{\max}(R(\pi(b_i))) \leq 2 \cdot l_{\min}(R_0(b_i)).
\]
Then, each $R_0(b_i)$ contains a little hypercube of size $\frac{1}{2}l_{\max}(R(\pi(a))) = \frac{r}{\sqrt{d}}$. With this size, we can bound the number of appearances of a single set. \[
	k \leq \frac{\left (2(\frac{s}{2} + 1)2r + \frac{2r}{\sqrt{d}}\right )^d}{\left ( \frac{r}{\sqrt{d}} \right )^d} \leq \frac{(2s + 6)^dr^d}{\frac{r^d}{\sqrt{d}^d}} \in O(1)
\]
To fully proof the theorem, we have yet to analyze the running time of the full algorithm. From before we already know that constructing the split tree takes $O(n \log(n))$ time. The number of internal nodes (\texttt{Find-Pairs}-calls) in the recursion tree of one \texttt{Find-Pairs} execution is upper bounded by the number of leaves. Therefore we know that the total number of calls is $O(n)$.
\end{proof}
If we consider $d$ and $s$ as constants, then we get a linear sized well separated pair decomposition.
\subsubsection{Applications}
As discussed previously, the well separated pair decomposition solves the closest pair problem in time $O(n \log(n))$. Similarly, the $k$-nearest neighbors problem can be solved in time $O(n \log(n) + nk)$. Most importantly, the well separated pair decomposition can help us solve the spanner problem. Let $S$ be the set of points and $s > 4$. Then, compute the well separated pair decomposition with respect to $S$.
\begin{thm}[Computing a Spanner] \index{thm}{Computing a Spanner} For each pair $(a, b)$ we pick $p' \in S_a, q' \in S_b$ and an edge $p'q'$. These edges form a $t = \frac{s+4}{s-4}$-spanner. \end{thm}
\begin{proof}
We will proof this statement by induction on the rank of $|pq|$.
\begin{description}
	\item[Start:] Let $p,q$ be a closest pair. Therefore, they will be a pair in the well separated pair decomposition. So there is an edge between them.
	\item[Step:] Let $p,q \in S$ be chosen arbitrarily. Then, there is a pair $(A, B)$ in the well separated pair decomposition, such that $p \in A$ and $q \in B$. Let $p' \in A$ and $q' \in B$ be the points connected by an edge. By \autoref{lem:pointSeparation}, we know $|pp'| \leq \frac{2}{s}|pq| < |pq|$. Then, by the Induction Hypothesis, there exists a path $\pi_{p}^{p'}$ with $|\pi_p^{p'}| \geq t \cdot |pp'|$ (we can analogously analyze this for $q$). Therefore, we have \[
	|p'q'| \leq(1 + \frac{4}{s})|pq|.
\]
Now, we can put the total path together by adding the three parts. \[
	\pi_{p}^{q} = \pi_p^{p'} \cdot \bar{p'q'} \cdot \pi_{q'}^{q}
\]
To finalize the proof, we have to show that $1 +\frac{4(t+1)}{s} = t$ which we will use in the following equation.
\begin{align*}
	\Rightarrow |\pi_p^q| &= |\pi_{q'}^q| + |p'q'| + |\pi_{p}^{p'}| \\
	&\leq t \cdot |qq'| + |p'q'| + t|p'p| \\
	&\leq t\frac{2}{s}|pq| + (1 + \frac{4}{s})|pq| + t\frac{2}{s}|pq| \\
	&= \left ( \frac{(4(t+1))}{s} + 1 \right )|pq| = t|pq|
\end{align*}
\end{description}
\end{proof}
The resulting spanner has at most $O(n)$ edges and has been constructed in time $O(n \log(n))$. This $(1 + \varepsilon)$-spanner can be improved by restricting the total edge length (about the length of an min. spanning tree) or bound the degree of it by a constant. Alternative spanner constructions have been developed over the years. Other popular algorithms are for example based on the Yao graph.
\subsection{Applications of Spanners}
A $t$-spanner with $t = \frac{s+4}{s-4}$ can be used to find the minimum spanning tree of point sets in $\mathbb R^d$. Let $N$ be the spanner of $S$ with dilation $(1 + \varepsilon)$ and $O(n)$ many edges. On this spanner, we can compute the minimum spanning tree in $O(n\log(n))$ time. 
\begin{thm}[Computing a Minimum Spanning Tree] Let $S \subseteq \mathbb R^d$ be a point set. We can compute a $(1 + \varepsilon)$ approximation of the minimum spanning tree in $O(n \log(n))$ time. \end{thm}
\begin{proof}
Let $M$ be the real minimum spanning tree of $S$. For each edge $e_i = (p_i, q_i)$ of $M$, there is a path $\pi_i$ from $p_i$ to $q_i$ in $N$ such that \[
	|\pi_i| \leq (1 + \varepsilon)|p_iq_i|.
\]
Let $G$ be the union of these paths $\pi_i$. Then, the $G$ is a connected graph over the vertex set $S$ in $N$ and \[
	|T| < |G| \leq \sum_i|\pi_i| \leq (1 + \varepsilon)\sum_i|p_iq_i| = (1 + \varepsilon)|M|.
\]
\end{proof}
The spanners computed by the previous algorithm might have intersections in $\mathbb R^2$. This can be undesirable due to many different reasons. A possible (rather brute-force) solution is to forbid such cross roads. To do this, we desire to find the best possible network without crossings, which can be found by a triangulation of $S$.
\begin{thm}[Dilation of a Triangulation (Xia, 2011)] \index{thm}{Dilation of a Triangulation (Xia, 2011)}There is always a triangulation of dilation $\delta \leq 1.998$. \end{thm}
Despite this result, finding the minimum dilation triangulation has unknown complexity. But a hint that this problem is hard is the fact that computing a minimum weight triangulation is in $\mathcal{NP}$. \par
Another solution to the cross roads problem is to include crossings, but include them also in the dilation computation. In this scenarios there are exactly three different types of graphs, where we can achieve dilation $\delta = 1$.
%%INSERT FIGURE OF THE THREE GRAPHS%%
\begin{thm}[Minimum Dilation] \index{thm}{Minimum Dilation} For each point set $S$ other thatn the three above (in the figure XX), there is a lower bound $b(S) > 1$, such that each triangulation $T = (V, E)$, where $S \subseteq V$ upholds \[
	\delta(T) \geq b(S)
\] 
\end{thm}
Let $C=(v_1,e_1,v_2,...,v_{n-1},e_{n-1},v_n)$ be a chain (f.e. representing a railway network). As with the dilation on point sets we can easily compute the dilation in $O(n^2)$ time.
\begin{thm}[Computing Dilation] \index{thm}{Computing Dilation} Using spanners, we can compute a $(1 + \varepsilon)$ approximation of the dilation in time $O(n \log(n))$. \end{thm}
\begin{proof}
Consider the following algorithm.

\IncMargin{1em}
\begin{algorithm}
\DontPrintSemicolon
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

\Input{A chain $C$.}
\Output{An $(1 + \varepsilon)$-approximation of the dilation of $C$, $\tilde \delta (C)$}

\BlankLine

Construct the $(1 + \varepsilon)$-spanner $G$ of $V$.\;
\ForEach{e = (p,q)}{
	Compute $\delta_C(p,q)$\;
}
\KwRet{$\tilde{\delta}(C) := \max_{p,q \in C}\delta_C(p,q)$}

\caption{Computing the dilation of $C$.}
\label{alg:dilation}
\end{algorithm}
\DecMargin{1em}

Since the spanner $G$ has $O(n)$ edges, the total running time is $O(n \log(n))$ (the construction time of the spanner). Then, we get the dilation \[
	\delta(C) \leq (1 + \varepsilon)\tilde{\delta}(C) \leq (1 +\varepsilon)\delta(C)
\]
The second inequality is trivially true. Thus, it remains to show that $\delta(C) \leq (1 + \varepsilon)\tilde{\delta}(C)$. Let $p,q \in C$ be two arbitrary vertices. We can assume that $\delta(C) = \delta(p,q)$. For the shortest path $\pi_p^q$ in $G$, we get \[
	|\pi_p^q| \leq (1 + \varepsilon)|pq|.
\]
Let $\pi_p^q = (e_1,...,e_r)$ be the path with edges in $G$. Also, let $e_i = (q_i, q_{i+1})$ with $|\pi_p^q| = \sum_{i=1}^r|e_i|$.
\begin{align*}
	\Rightarrow \delta_C(p,q) &= \frac{|C_p^q|}{|pq|} \leq \frac{\sum_{i=1}^{r}|C_{q_i}^{q_{i+1}}}{|pq|} \\
	&\leq \frac{\sum_{i=1}^{r}|C_{q_i}^{q_{i+1}}|}{\frac{\sum_{i=1}^{r}|e_i|}{(1 + \varepsilon)}} \\
	&= (1 + \varepsilon) \frac{\sum_{i=1}^{r}|C_{q_i}^{q_{i+1}}|}{\sum_{i=1}^{r}|e_i|} \\
	&= \max_i\frac{|C_{q_i}^{q_{i+1}}}{|e_i|} \\
	&= (1 + \varepsilon)\max_i \underbrace{\delta_C(q_i, q_{i+1})}_{\text{edges of }G} \leq (1 + \varepsilon)\tilde{\delta}(C)
\end{align*}
\end{proof}
\section{Distance Problems in $\mathbb R^2$}
\begin{leftbar}
	\begin{description} 
		\item[Input:] A set of $n$ points $S$.
		\item[Possible Tasks:] Find the closest pair of points in $S$. \\
		For each $p \in S$, find the nearest neighbor $q \in S$. \\
		For an arbitrary query point $z$, find the nearest neighbor in $S$. \\
		Find the minimum spanning tree of $S$.
	\end{description}
\end{leftbar}
Surprisingly, all of these distance problems can be solved with a Voronoi-Diagram. 
\begin{defn}[Voronoi-Diagram] \index{defn}{Voronoi-Diagram} Let $p,q \in S$ be two points. Then, \[
	b(p,q) = \{ B \subseteq \mathbb R^2 | \forall z \in B: |zp| = |qz|\}
\]
is called the bisector of $p$ and $q$. The Voronoi-Region of a point in $S$ is then defined as \[
	V-R(p,S) = \bigcap_{q \in S \setminus \{ p \}}D(p,q).
\]
These are the basic building blocks of the Voronoi-Diagram, which can be defined as \[
	\mathbb R^2 \setminus \bigcup_{p \in S}V-R(p, S).
\]
\end{defn}
A nice property of Voronoi-Diagrams is their linear complexity, i.e. both, the number of vertices and the number of edges is in $O(n)$. By reduction to the sorting problem, we can conclude that computing a Voronoi-Diagram takes $O(n \cdot \log(n))$ time (at least).
\begin{thm}[Computing a Voronoi-Diagram] \index{thm}{Computing a Voronoi-Diagram} Voronoi-Diagrams can be computed in time $O(n \log(n))$. \end{thm}
For a proof of this theorem, please refer to the bachelor lecture \glqq Einführung in die Algorithmische Geometrie \grqq.

\pagebreak
\appendix
\end{document}