function tjrs = joint_tjrs()
    tjrs=zeros(2,2,2,2);
    r=p_r;
    s=p_s;
    j_r=cond_j_r;
    t_rs=cond_t_rs;
    for J=1:2
        for R=1:2
            for S=1:2
                tjrs(:,J,R,S)=t_rs(:,R,S)*j_r(J,R)*r(R)*s(S);
            end
        end
    end
end

