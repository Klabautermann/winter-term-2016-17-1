function r = p_r()
    r=zeros(2,1);
    r(2)=1-0.2; % R=0
    r(1)=0.2;   % R=1
end