function s_tj = cond_s_tj()
    tjrs=joint_tjrs;
    % marginalization
    tjs=squeeze(sum(tjrs,3));
    tj=squeeze(sum(tjs,3));
    s_tj=zeros(2,2,2);
    % definition of cond. prob.
    for T=1:2
        for J=1:2
            s_tj(:,T,J)=tjs(T,J,:)/tj(T,J);
        end
    end
end
