function j_r = cond_j_r()
    j_r=zeros(2,2);
    j_r(1,1)=1;     % J=1, R=1
    j_r(1,2)=0.2;   % J=1, R=0
    j_r(2,1)=1-1;   % J=0, R=1
    j_r(2,2)=1-0.2; % J=0, R=0
end

