function s_t = cond_s_t()
    st=joint_st;
    % use marginalization
    t=sum(st,1);
    % use Bayes' rule
    s_t=zeros(2,2);
    for T=1:2
        s_t(:,T)=st(:,T)/t(T);
    end
end